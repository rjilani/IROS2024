import matplotlib.pyplot as plt
import pickle
import numpy as np

def get_storage(filename):
    with open('StorageData/carotid_artery_insertion/'+filename, 'rb') as file: 
        storage = pickle.load(file)
    return storage


if __name__ == "__main__":
    #paste .pkl file name
    filename = "29bf6010-c5cd-41cc-ab03-4a0f8317b03d_carotid_artery_insertion.pkl" 
    storage0 =  get_storage(filename)

    extime  = storage0.SM_timeu_ + storage0.SM_time_
    print("Carotid artery insertion:")
    print("Avg. BVP solver time", sum(extime)/200)
    print("Avg. contact detection time", sum(storage0.CD_time_)/200)
    print("Avg. #blobs", storage0.CD_surface.avg_nb_blobs)

