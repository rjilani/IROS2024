
import numpy as np
from scipy.spatial.transform import Rotation as Rotation
import os
import pickle
import uuid
class Storage:
    def __init__(self, hist, MP, BDF, CD_surface, VZ_surface, k, return_folder_name, non_unique_name, directory):
        self.hist = hist
        self.Nt = hist.Nt
        self.N = hist.N
        self.MP = MP
        self.BDF = BDF
        self.k =  k
        self.CD_surface = CD_surface
        self.VZ_surface = VZ_surface

        self.p_ = hist.Yc_[:,:,0:3]
        self.R_ = hist.Yc_[:,:,3:12]
        self.n_ = hist.Yc_[:,:,12:15]
        self.m_ = hist.Yc_[:,:,15:18]
        self.q_ = hist.Yc_[:,:,18:21]
        self.w_ = hist.Yc_[:,:,21:24]
        self.ut_ = hist.Ztc_[:,:,0:3]
        self.qt_ = hist.Ztc_[:,:,3:6]
        self.wt_ = hist.Ztc_[:,:,6:9]
        self.vt_ = hist.Ztc_[:,:,9:12]

        self.pu_ = hist.Yu_[:,:,0:3]
        self.Ru_ = hist.Yu_[:,:,3:12]
        self.nu_ = hist.Yu_[:,:,12:15]
        self.mu_ = hist.Yu_[:,:,15:18]
        self.qu_ = hist.Yu_[:,:,18:21]
        self.wu_ = hist.Yu_[:,:,21:24]
        self.utu_ = hist.Ztu_[:,:,0:3]
        self.qtu_ = hist.Ztu_[:,:,3:6]
        self.wtu_ = hist.Ztu_[:,:,6:9]
        self.vtu_ = hist.Ztu_[:,:,9:12]

        self.fc_  = hist.fc_

        self.SM_time_ = hist.SM_timec_
        self.SM_timeu_ = hist.SM_timeu_
        self.CD_time_ = hist.CD_time_
        self.CR_time_ = hist.CR_time_

        self.success_ = np.zeros(hist.Nt)
        self.nfev_   = np.zeros(hist.Nt)
        self.successu_ = np.zeros(hist.Nt)
        self.nfevu_    = np.zeros(hist.Nt)
        self.N_in_col_ = np.zeros(hist.Nt)

        for i in range(hist.Nt):
            self.success_[i] = hist.solc_[i].success
            self.successu_[i] = hist.solu_[i].success
            self.nfev_[i] = hist.solc_[i].nfev
            self.nfevu_[i] = hist.solu_[i].nfev
            self.N_in_col_[i] = np.sum(hist.in_col_[i])
            
        self.folder_name = self.save(directory,non_unique_name)
        return_folder_name.append(self.folder_name)

    def plot_steps(self, j, ls, c, c2, plt, arr_fig_ax = None):
        xyz = ["x", "y", "z"]
        y   = ["val"]
        d123 = ["d1x","d2x","d3x","d1y","d2y","d3y","d1z","d2z","d3z"]

        x_axis = "time steps"
        ls2 = ":"

        _0T = np.arange(0, self.Nt)
        components = [[self.p_[:,j],xyz,"p",_0T,None,0,ls,c],
                    [self.R_[:,j],d123,"R",_0T,None,1,ls,c],
                    [self.n_[:,j],xyz,"n",_0T,None,2,ls,c],
                    [self.m_[:,j],xyz,"m",_0T,None,3,ls,c],
                    [self.q_[:,j],xyz,"q",_0T,None,4,ls,c],
                    [self.w_[:,j],xyz,"w",_0T,None,5,ls,c],
                    [self.ut_[:,j],xyz,"ut",_0T,None,6,ls,c],
                    [self.qt_[:,j],xyz,"qt",_0T,None,7,ls,c],
                    [self.wt_[:,j],xyz,"wt",_0T,None,8,ls,c],
                    [self.vt_[:,j],xyz,"vt",_0T,None,9,ls,c],
                    [self.fc_[:,j],xyz,"fc",_0T,None,10,ls,c],
                    [self.success_,y,"success",_0T,None,11,ls,c],
                    [self.nfev_,y,"xi (nfev)",_0T,None,12,ls,c],
                    [self.SM_time_,y,"BVP solver time (constrained)",_0T,None,13,ls,c],
                    [self.N_in_col_,y,"#points in contact",_0T,None,14,ls,c],
                    # uncomment to plot unconstrained variables
                    # [self.pu_[:,j],xyz,"p",_0T,None,0,ls2,c2],
                    # [self.Ru_[:,j],d123,"R",_0T,None,1,ls2,c2],
                    # [self.nu_[:,j],xyz,"n",_0T,None,2,ls2,c2],
                    # [self.mu_[:,j],xyz,"m",_0T,None,3,ls2,c2],
                    # [self.qu_[:,j],xyz,"q",_0T,None,4,ls2,c2],
                    # [self.wu_[:,j],xyz,"w",_0T,None,5,ls2,c2],
                    # [self.utu_[:,j],xyz,"ut",_0T,None,6,ls2,c2],
                    # [self.qtu_[:,j],xyz,"qt",_0T,None,7,ls2,c2],
                    # [self.wtu_[:,j],xyz,"wt",_0T,None,8,ls2,c2],
                    # [self.vtu_[:,j],xyz,"vt",_0T,None,9,ls2,c2],
                    # [self.successu_,y,"success",_0T,None,11,ls2,c2],
                    # [self.nfevu_,y,"xi (nfev)",_0T,None,12,ls2,c2],
                    # [self.SM_timeu_,y,"BVP solver time (constrained and unconstrained)",_0T,None,13,ls2,c2],
                ]
   
        if arr_fig_ax == None:
            arr_fig_ax = []
            for ind in range(len(components)):
                if components[ind][5] >= ind :
                    fig, ax = plt.subplots(len(components[ind][1]), 1)
                    arr_fig_ax.append((fig,ax)) 

        for ind in range(len(components)):
            comp = components[ind]
            fig = self.plot_components(comp[0], comp[1], comp[2], comp[3], comp[4], arr_fig_ax[comp[5]], comp[6], comp[7], x_axis)

        return arr_fig_ax
    
    def plot_step(self, i, ls, c, c2, plt, arr_fig_ax =None):
        xyz = ["x", "y", "z"]
        d123 = ["d1x","d2x","d3x","d1y","d2y","d3y","d1z","d2z","d3z"]
        _0L = np.linspace(0, self.MP["L"], self.N)
        
        s = "s"
        ls2 = ":"

        components = [[self.p_[i],xyz,"p",_0L,None,0,ls,c],
                      [self.R_[i],d123,"R",_0L,None,1,ls,c],
                      [self.n_[i],xyz,"n",_0L,None,2,ls,c],
                      [self.m_[i],xyz,"m",_0L,None,3,ls,c],
                      [self.q_[i],xyz,"q",_0L,None,4,ls,c],
                      [self.w_[i],xyz,"w",_0L,None,5,ls,c],
                      [self.ut_[i],xyz,"ut",_0L,None,6,ls,c],
                      [self.qt_[i],xyz,"qt",_0L,None,7,ls,c],
                      [self.wt_[i],xyz,"wt",_0L,None,8,ls,c],
                      [self.vt_[i],xyz,"vt",_0L,None,9,ls,c],
                      [self.fc_[i],xyz,"fc",_0L,None,10,ls,c],
                      # uncomment to plot unconstrained variables
                    #   [self.pu_[i],xyz,"p",_0L,None,0,ls2,c2],
                    #   [self.Ru_[i],d123,"R",_0L,None,1,ls2,c2],
                    #   [self.nu_[i],xyz,"n",_0L,None,2,ls2,c2],
                    #   [self.mu_[i],xyz,"m",_0L,None,3,ls2,c2],
                    #   [self.qu_[i],xyz,"q",_0L,None,4,ls2,c2],
                    #   [self.wu_[i],xyz,"w",_0L,None,5,ls2,c2],
                    #   [self.utu_[i],xyz,"ut",_0L,None,6,ls2,c2],
                    #   [self.qtu_[i],xyz,"qt",_0L,None,7,ls2,c2],
                    #   [self.wtu_[i],xyz,"wt",_0L,None,8,ls2,c2],
                    #   [self.vtu_[i],xyz,"vt",_0L,None,9,ls2,c2],
                        ]
        
        if arr_fig_ax == None:
            arr_fig_ax = []
            for ind in range(len(components)):
                if components[ind][5] >= ind :
                    fig, ax = plt.subplots(len(components[ind][1]), 1)
                    arr_fig_ax.append((fig,ax)) 

        for ind in range(len(components)):
            comp = components[ind]
            fig = self.plot_components(comp[0], comp[1], comp[2], comp[3], comp[4], arr_fig_ax[comp[5]], comp[6], comp[7], s)

        print("------------------------------------")
        print("Unconstrained:")
        print("success:", int(self.successu_[i])==1)
        print("xi (nfev):", int(self.nfevu_[i]))
        print("BVP solver time:",self.SM_timeu_[i])
        print("------------------------------------")
        print("Constrained:")
        print("success:", int(self.success_[i])==1)
        print("xi (nfev):", int(self.nfev_[i]))
        print("BVP solver time:",self.SM_time_[i])
        print("------------------------------------")
        print("contact detection time", self.CD_time_[i])
        print("contact response time", self.CR_time_[i])
        print("#points in contact",int(self.N_in_col_[i]))
        print("------------------------------------")

        return arr_fig_ax

    def plot_single_component(self, ax, data, label, time_axis, linestyle, color, step_or_s):
        ax.plot(time_axis, data, label=label, linestyle=linestyle, color=color)
        ax.set_xlabel(step_or_s)
        ax.set_ylabel(label)

    def plot_multi_components(self, ax, data, labels, time_axis, linestyle, color,step_or_s):
        for i, label in enumerate(labels):
            ax[i].plot(time_axis, data[:, i], label=label, linestyle=linestyle, color=color)
            ax[i].set_xlabel(step_or_s)
            ax[i].set_ylabel(label)

    def plot_components(self, data, labels, title, time_axis, elements, fig_ax, linestyle, color, step_or_s):
        fig, ax = fig_ax
        if data.ndim == 1:
            self.plot_single_component(ax, data, labels[0], time_axis, linestyle, color, step_or_s)
        elif data.ndim == 2:
            self.plot_multi_components(ax, data, labels, time_axis, linestyle, color, step_or_s)
        elif data.ndim == 3:
            for id, j in enumerate(elements):
                self.plot_multi_components(ax, data[:, j, :], labels, time_axis, linestyle, color, step_or_s)

        fig.suptitle(title)
        return fig
    
    def save(self, directory, non_unique_name):
        name = str(uuid.uuid4())+non_unique_name
        file_path = os.path.join(directory, name)+".pkl"
        with open(file_path, "wb") as f:
            pickle.dump(self, f)
        print("Storage object is stored in", file_path)
        return name
    