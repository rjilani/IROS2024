import plotly.graph_objects as go
import numpy as np
import pickle
import plotly.io as pio
import uuid

def load_data(storage_path):
    with open(storage_path + '.pkl', 'rb') as file:
        storage = pickle.load(file)
    return storage

def setup_figure(frames, range):
    fig = go.Figure(data=frames[0].data)
    fig.frames = frames
    fig.update_layout(
        title='IROS2024', 
        margin=dict(l=0, r=0, b=0, t=0),
        scene=dict(
            # xaxis=dict(title='x [m]', range=[-range, range], showgrid=False, zeroline=False, showticklabels=False, backgroundcolor='rgba(0,0,0,0)'),
            # yaxis=dict(title='y [m]', range=[-range, range], showgrid=False, zeroline=False, showticklabels=False, backgroundcolor='rgba(0,0,0,0)'),
            # zaxis=dict(title='z [m]', range=[-range, range], showgrid=False, zeroline=False, showticklabels=False, backgroundcolor='rgba(0,0,0,0)'),
            
            xaxis=dict(title='x [m]', range=[-range, range], showgrid=True),
            yaxis=dict(title='y [m]', range=[-range, range], showgrid=True),
            zaxis=dict(title='z [m]', range=[-range, range], showgrid=True),
            
            aspectmode='manual',
            aspectratio=dict(x=1, y=1, z=1),
            # camera=dict(
            #     eye=dict(x=-1, y=0, z=0),  
            #     up=dict(x=0, y=0, z=1),  
            #     center=dict(x=0, y=0, z=0),  
            # )
        ),
        legend=dict(
            x=1.05,  
            y=0.5,   
            xanchor='left',  
            yanchor='bottom'  
        ),
        sliders=[{
            'pad': {"t": 0},
            'steps': [{'args': [[f.name], {'frame': {'duration': 500, 'redraw': True}, 'mode': 'immediate', 'fromcurrent': True}],
                      'label': f.name, 'method': 'animate'} for f in fig.frames]
        }],
        updatemenus=[{
            'type': 'buttons',
            'showactive': True,
            'y': 0,
            'x': 0,
            'xanchor': 'right',
            'yanchor': 'top',
            'pad': {'t': 0, 'r': 0},
            'buttons': [
                {'args': [None, {'frame': {'duration': 500, 'redraw': True}, 'fromcurrent': True, 'transition': {'duration': 300, 'easing': 'linear'}}],
                 'label': 'Play', 'method': 'animate'},
                {'args': [[None], {'frame': {'duration': 0, 'redraw': True}, 'mode': 'immediate', 'transition': {'duration': 0}}],
                 'label': 'Pause', 'method': 'animate'}
            ]
        }],
    )
    return fig

def go_scatter3d(v_s, color, width, name, colorscale=None, cmin=None, cmax=None):
        scatter3d = go.Scatter3d(x=v_s[:, 0],
                     y=v_s[:, 1],
                     z=v_s[:, 2],
                     mode='lines',
                     name = name,
                     line=dict(color=color, colorscale=colorscale, width=width, cmin=cmin, cmax=cmax),
                     showlegend=True
                    )
        return scatter3d

def go_cone(p_s, v_s, color, sizeref, name):
        cone = go.Cone(
                x=p_s[:,0], y=p_s[:,1], z=p_s[:,2], #Apex position
                u=v_s[:,0], v=v_s[:,1], w=v_s[:,2], #Cone direction
                colorscale = [[0, color], [1, color]], showscale = False,
                sizemode='scaled', sizeref=sizeref,showlegend=True,
                name = name,
            )
        return cone

def create_rot_vectors(p,R,Nn,N,rot_scale):
    R = R.reshape(-1,3,3)
    ppp = np.tile(p, (3, 1))
    scaled_d1d2d3 = np.concatenate([R[:,:,0],R[:,:,1],R[:,:,2]])*rot_scale
    ppp_plus_d1d2d3 = ppp+scaled_d1d2d3
    ppp_to_ppp_plus_d1d2d3 = np.array([zp for zp in zip(ppp,ppp_plus_d1d2d3,Nn)]).reshape(N*9,-1)
    return ppp_to_ppp_plus_d1d2d3, ppp_plus_d1d2d3, scaled_d1d2d3

def create_fc_vectors(p, normalized_fc, norm_fc, max_norm_fc_, none_fc, N,fc_scale):
    norm_fc = np.tile(norm_fc,(3,1)).T
    if max_norm_fc_ > 0:
        scaled_fc = normalized_fc*norm_fc*fc_scale/max_norm_fc_
    else:
        scaled_fc = normalized_fc*norm_fc*fc_scale
    p_plus_scaled_fc = p+scaled_fc
    p_to_p_plus_scaled_fc = np.array([zp for zp in zip(p,p_plus_scaled_fc,none_fc)]).reshape(N*3,-1)
    return p_to_p_plus_scaled_fc, p_plus_scaled_fc, scaled_fc

def compute_colors(n, nu, fc, N,min_n_norm,max_n_norm):
    norm_fc = np.linalg.norm(fc, axis=1)
    n_norm = np.linalg.norm(n, axis=1)
    nu_norm = np.linalg.norm(nu, axis=1)
    max_norm_fc = np.max(norm_fc)
    min_norm_fc = np.min(norm_fc)
    max_nu_norm = np.max(nu_norm)
    min_nu_norm = np.min(nu_norm)

    fc_colors = (norm_fc - min_norm_fc) / (max_norm_fc - min_norm_fc) if max_norm_fc > 0 else np.zeros(N)
    n_colors = (n_norm - min_n_norm) / (max_n_norm - min_n_norm) if max_n_norm > 0 else np.zeros(N)
    nu_colors = (nu_norm - min_nu_norm) / (max_nu_norm - min_nu_norm) if max_nu_norm > 0 else np.zeros(N)

    normalized_fc = np.divide(fc, norm_fc[:, np.newaxis], where=norm_fc[:, np.newaxis] != 0)

    return n_norm, n_colors, nu_norm, nu_colors, normalized_fc, norm_fc, fc_colors, max_norm_fc

def create_frames(p_,R_,n_,pu_,Ru_,nu_,fc_, Nt, N, rot_scale, fc_scale, width, sizeref):
    custom_colorscale = [
        [0.0, "#0000ff"],   # Blue 
        [0.25, "#00ffff"],  # Cyan 
        [0.5, "#ffff00"],   # Yellow 
        [1.0, "#ff0000"]    # Red 
    ]
    
    none_rot = np.empty((N*3,3),dtype=object)
    none_fc  = np.empty((N,3),dtype=object)

    n_norms = np.linalg.norm(n_, axis=2)
    max_n_norm_index = np.unravel_index(np.argmax(n_norms), n_norms.shape)
    max_n_norm = n_norms[max_n_norm_index[0], max_n_norm_index[1]]
    min_n_norm_index = np.unravel_index(np.argmin(n_norms), n_norms.shape)
    min_n_norm = n_norms[min_n_norm_index[0], min_n_norm_index[1]]

    frames = []
    for tstep in range(Nt):
        p  = p_[tstep]
        R  = R_[tstep]
        n  = n_[tstep]

        pu  = pu_[tstep]
        Ru  = Ru_[tstep]
        nu  = nu_[tstep]

        fc = fc_[tstep]
        
        n_norm, n_colors, nu_norm, nu_colors, normalized_fc, norm_fc, fc_colors, max_norm_fc = compute_colors(n,nu,fc,N,min_n_norm,max_n_norm)
        n_colors = np.zeros_like(n_colors) #make the rod all blue

        #--------------------------------------------------------------------------
        pR, ppp_plus_d1d2d3, scaled_d1d2d3 = create_rot_vectors(p,R,none_rot,N,rot_scale)
        puRu, ppp_plus_d1d2d3u, scaled_d1d2d3u  = create_rot_vectors(pu,Ru,none_rot,N,rot_scale)
        p_to_p_plus_scaled_fc, p_plus_scaled_fc, scaled_fc = create_fc_vectors(p, normalized_fc, norm_fc, max_norm_fc, none_fc, N, fc_scale)

        #--------------------------------------------------------------------------
        frame_data = []
        # uncomment to plot unconstrained positions and orientations
        # frame_data = [go_scatter3d(pu,  "rgba(0,0,0,0.3)", width, "pu")]
        # frame_data.extend([go_scatter3d(puRu, "rgba(0,0,0,0.3)", width/2, "Ru",None,None,None)])
        # frame_data.extend([go_cone(ppp_plus_d1d2d3u,scaled_d1d2d3u, "rgba(0,0,0,0.3)", sizeref,"Ru")])

        frame_data.extend([go_scatter3d(p, n_colors, width, "p",custom_colorscale, 0, 1)])
        frame_data.extend([go_scatter3d(pR, "rgba(0,0,0)", width/2,"R",None,None,None)])
        frame_data.extend([go_cone(ppp_plus_d1d2d3,scaled_d1d2d3, "rgba(0,0,0)", sizeref,"R")])

        fc__color = np.array([z for z in zip(fc_colors,fc_colors,fc_colors)]).reshape(-1)
        frame_data.extend([go_scatter3d(p_to_p_plus_scaled_fc, fc__color, width/2, "fc", custom_colorscale, 0, 1)])
        frame_data.extend([go_cone(p_plus_scaled_fc,scaled_fc, "rgba(0,0,0,1)", sizeref, "fc")])

        new_frame = go.Frame(data=frame_data, name=str(tstep))
        frames.append(new_frame)
    
    return frames

def cantilever_rod_in_frictional_contact_plot_3D(storage_path, unique_name, html_filename, CD_surface, color, opacity):
    storage = load_data(storage_path)
    
    Nt = storage.Nt
    N = storage.N
    L = storage.MP["L"]
    
    p_ = storage.p_
    R_ = storage.R_
    n_  = storage.n_
    pu_ = storage.pu_
    Ru_ = storage.Ru_
    nu_  = storage.nu_ 
    fc_ = storage.fc_

    rot_scale = L/(N*3)
    fc_scale  = rot_scale*5
    width     = L*5
    sizeref   = width/25

    frames = create_frames(p_,R_,n_,pu_,Ru_,nu_,fc_,Nt,N,rot_scale, fc_scale, width, sizeref)
    fig = setup_figure(frames, L*1.5)
    goMesh3d = CD_surface.plot_3D(color, opacity)
    fig.add_trace(goMesh3d)

    # fig.write_html("_HTML/cantilever_rod_in_frictional_contact/"+html_filename+".html")
    fig.write_html("_HTML/cantilever_rod_in_frictional_contact/"+unique_name+".html")
    print("3D animation is stored in","_HTML/cantilever_rod_in_frictional_contact/"+unique_name+".html")

def carotid_artery_insertion_plot_3D(storage_path, unique_name, html_filename, CD_surface, color, opacity):
    storage = load_data(storage_path)
    
    Nt = storage.Nt
    N = storage.N
    L = storage.MP["L"]

    p_ = storage.p_
    R_ = storage.R_
    n_  = storage.n_
    pu_ = storage.pu_
    Ru_ = storage.Ru_
    nu_  = storage.nu_
    fc_ = storage.fc_

    rot_scale = L/(N*3)
    fc_scale  = rot_scale*5
    width     = L*120
    sizeref   = width/25

    frames = create_frames(p_,R_,n_,pu_,Ru_,nu_,fc_,Nt,N,rot_scale, fc_scale, width, sizeref)
    fig = setup_figure(frames, L*2)

    goMesh3d = CD_surface.plot_3D(color, opacity)
    fig.add_trace(goMesh3d)

    # fig.write_html("_HTML/carotid_artery_insertion/"+html_filename+".html")
    fig.write_html("_HTML/carotid_artery_insertion/"+unique_name+".html")
    print("3D animation is stored in","_HTML/carotid_artery_insertion/"+unique_name+".html")

def helical_insertion_plot_3D(storage_path, unique_name, html_filename, VZ_surface, color, opacity):
    storage = load_data(storage_path)
   
    Nt = storage.Nt
    N = storage.N
    L = storage.MP["L"]

    p_ = storage.p_
    R_ = storage.R_
    n_  = storage.n_
    pu_ = storage.pu_
    Ru_ = storage.Ru_
    nu_  = storage.nu_ 
    fc_ = storage.fc_
    
    rot_scale = L/(N*3)
    fc_scale  = rot_scale*5
    width     = L*40
    sizeref   = width/25

    frames = create_frames(p_,R_,n_,pu_,Ru_,nu_,fc_,Nt,N,rot_scale, fc_scale, width, sizeref)
    fig = setup_figure(frames, L*2)

    goMesh3d = VZ_surface.plot_3D(color, opacity)
    fig.add_trace(goMesh3d)

    # fig.write_html("_HTML/helical_insertion/"+html_filename+".html")
    fig.write_html("_HTML/helical_insertion/"+unique_name+".html")
    print("3D animation is stored in","_HTML/helical_insertion/"+unique_name+".html")


if __name__ == "__main__":
    pass