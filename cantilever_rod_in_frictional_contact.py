import numpy as np
from Simulation import *
from Cosserat.RodProperties import *
from Storage import Storage
from plot_3D import cantilever_rod_in_frictional_contact_plot_3D
from Cosserat.History import *
from Geometry.Sphere import Sphere 
from Cosserat.BDF2 import BDF2

mu = 0
# mu = 0.1 
# mu = 0.2 
# mu = 0.3 
# mu = 0.4 

#-----------------------------------------------------------------------------------------------

Bbt = 5e-3*np.eye(3) 
Bse = 5e-4*np.eye(3) 
dt = 0.005    
Nt = 200     
N = 200 

eps = 0.001 
k = 500
sphere_rad = 0.01

sphere_pos = 0.9
dynamic_g = np.array([9.81,0,0])
L = 1      
r = 0.005  
nu = 0.5   
E = 5e7    
rho = 1100 
gamma = 5e-6 

CD_surface = Sphere(np.array([sphere_rad,0,sphere_pos]), sphere_rad, intersect_if_outside=False)
g = np.zeros(3)
p0 = np.zeros(3)
R0 = np.eye(3)
usr = np.array([0,0,0]) 
vsr = np.array([0,0,1])
nL = np.zeros(3)
mL = np.zeros(3)

BDF = BDF2(dt)
MP = get_RP(L, r, E, nu, rho, g, usr, vsr, Bbt, Bse, BDF.c0)
hist = History(p0,R0,nL,mL,Nt,N)

run_cantilever_rod_in_frictional_contact(hist,MP,BDF,CD_surface,k,mu,eps,gamma,dynamic_g)

return_folder_name = []
non_unique_name    = "_cantilever_rod_in_frictional_contact_"+str(mu)
directory = "StorageData/cantilever_rod_in_frictional_contact/"
Storage(hist, MP, BDF, CD_surface, CD_surface, k, return_folder_name, non_unique_name, directory)
storage_path = directory+return_folder_name[0]

cantilever_rod_in_frictional_contact_plot_3D(storage_path, return_folder_name[0], non_unique_name, CD_surface, color = "green", opacity = 1)
