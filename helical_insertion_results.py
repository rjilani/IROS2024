import matplotlib.pyplot as plt
import pickle
import Storage
import numpy as np

def get_storage(filename):
    with open('StorageData/helical_insertion/'+filename, 'rb') as file: #20
        storage = pickle.load(file)
    return storage

def custom_print(*args, **kwargs):
    num_decimals = 4
    args = [f"{arg:.{num_decimals}f}" if isinstance(arg, float) else arg for arg in args]
    print(*args, **kwargs)

def print_CD_time(storage0,storage1,storage2,storage3,storage4):
    print = custom_print
    print("-------------------------------------------------------")
    print("Avg. contact detection time")
    print("blobby model", sum(storage0.CD_time_)/200)
    print("mesh 1.5k", sum(storage4.CD_time_)/200)
    print("mesh 7k", sum(storage3.CD_time_)/200)
    print("mesh 29k", sum(storage2.CD_time_)/200)
    print("mesh 65k", sum(storage1.CD_time_)/200)

def print_nfev(storage0, storage1, storage2, storage3, storage4):
    sum_nfev0 = sum(storage0.nfevu_+storage0.nfev_)
    sum_nfev1 = sum(storage1.nfevu_+storage1.nfev_)
    sum_nfev2 = sum(storage2.nfevu_+storage2.nfev_)
    sum_nfev3 = sum(storage3.nfevu_+storage3.nfev_)
    sum_nfev4 = sum(storage4.nfevu_+storage4.nfev_)
    print("-------------------------------------------------------")
    print("Number of evaluations of the objective function (xi)")
    print("blobby model",int(sum_nfev0))
    print("mesh 1.5k",int(sum_nfev4))
    print("mesh 7k",int(sum_nfev3))
    print("mesh 29k",int(sum_nfev2))
    print("mesh 65k",int(sum_nfev1))

def plot_mesh_error(storage0, storage1, storage2, storage3, storage4, savefig = False):
    fig, ax = plt.subplots()
    error = np.zeros((4,200))
    L = 0.26
    time_axis = np.linspace(0,1,201)
    for i in range(200):
        pL_si = storage0.p_[i,-1]
        pL_m4 = storage4.p_[i,-1]
        pL_m3 = storage3.p_[i,-1]
        pL_m2 = storage2.p_[i,-1]
        pL_m1 = storage1.p_[i,-1]
        error[0,i] = (np.linalg.norm(pL_si-pL_m4)/L)*100
        error[1,i] = (np.linalg.norm(pL_si-pL_m3)/L)*100
        error[2,i] = (np.linalg.norm(pL_si-pL_m2)/L)*100
        error[3,i] = (np.linalg.norm(pL_si-pL_m1)/L)*100
    #Add 0 since at rest all simulations are the same
    ax.plot(time_axis,np.concatenate([[0],error[0]]),color="C3",linestyle=":",label="mesh 1.5k")
    ax.plot(time_axis,np.concatenate([[0],error[1]]),color="C2",linestyle="-.",label="mesh 7k")
    ax.plot(time_axis,np.concatenate([[0],error[2]]),color="C1",linestyle="--",label="mesh 29k")
    ax.plot(time_axis,np.concatenate([[0],error[3]]),color="C0",linestyle="-",label="mesh 65k")
    ax.set_xlabel('time [s]')
    ax.set_ylabel(r'$e_p (\%)$')
    ax.legend()
    ax.grid()
    if savefig:
        fig.savefig("_images\helical_insertion\mesh_errors.eps", dpi=300, bbox_inches='tight')
        fig.savefig("_images\helical_insertion\mesh_errors.png", dpi=300, bbox_inches='tight')

def plot_nL(storage0, storage1, savefig = False):
    fig, ax = plt.subplots()
    time_x   = np.linspace(0,1,201)
    storage0n = np.concatenate([[0],storage0.n_[:,-1,0]])
    storage1n = np.concatenate([[0],storage1.n_[:,-1,0]])
    ax.plot (time_x,storage1n,color="C1",linestyle="-", label="mesh 65k")
    ax.plot (time_x,storage0n,color="C0",linestyle="-",label="blobby model",)
    ax.set_xlabel('time [s]')
    ax.set_ylabel(r'$n_x(t_i,L)$')
    ax.legend()
    ax.grid()
    if savefig:
        fig.savefig("_images\helical_insertion\internal_force_L.eps", dpi=300, bbox_inches='tight')
        fig.savefig("_images\helical_insertion\internal_force_L.png", dpi=300, bbox_inches='tight')

if __name__ == "__main__":
    #paste .pkl files name
    filename0 = "645f2b49-70ac-4190-8b5b-0ceafda1b71c_helical_insertion_blobby_model.pkl" # copy blobby model
    filename1 = "8dadde7b-f9ef-4f6d-86a2-2b1ef824ec59_helical_insertion_mesh_L_150.pkl"   # copy mesh L/150
    filename2 = "1fc1398c-a1f1-42bc-9578-d9401d5e5b0d_helical_insertion_mesh_L_100.pkl"   # copy mesh L/100
    filename3 = "58b98e07-cc63-48a0-b358-cb5f1f898fb9_helical_insertion_mesh_L_50.pkl"    # copy mesh L/50
    filename4 = "f158cfc9-1dfc-4500-91ac-d0db2601baa5_helical_insertion_mesh_L_25.pkl"    # copy mesh L/25

    storage0 =  get_storage(filename0)
    storage1 =  get_storage(filename1)
    storage2 =  get_storage(filename2)
    storage3 =  get_storage(filename3)
    storage4 =  get_storage(filename4)

    time0 = sum(storage0.SM_timeu_+storage0.SM_time_)/200
    print("Helical insertion:")
    print("BVP solver time for blobby model version", time0)
    print_nfev(storage0, storage1, storage2, storage3, storage4)
    print_CD_time(storage0, storage1, storage2, storage3, storage4)
    plot_mesh_error(storage0, storage1, storage2, storage3, storage4, savefig=False)
    plot_nL(storage0, storage1, savefig=False)
    plt.show()
