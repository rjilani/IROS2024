import numpy as np
from Cosserat.RodProperties import *
from Simulation import *
from plot_3D import carotid_artery_insertion_plot_3D
from Geometry.MyTrimesh import MyTrimesh
from Geometry.BlobbyCarotid import BlobbyCarotid
from Storage import Storage
from Cosserat.History import *


L = 0.08
r = 0.0003
E = 5.5e+5
nu = 0.45
rho = 1100
Bbt = 5e-10*np.eye(3) 
Bse = 5e-11*np.eye(3) 

dt = 0.005
p0 = np.array([0.004,0,-L+0.01376]) 

Nt = 200
N = 50 
k = 0.1      

blobby_carotid = BlobbyCarotid(N)
CD_surface = blobby_carotid
stl_path = "_Stl/carotid_insertion/carotid_tanslated_scaled"
VZ_surface = MyTrimesh(None,None,None,stl_path) #Visualization surface

gamma = 5e-6
mu = 0.05 
eps = L/1000 

R0 = np.eye(3)@rotation_matrix("y", -4.5) 
dp = np.tile(R0@np.array([0,0,3.2e-4]),Nt).reshape((-1,3)) 

nL = np.zeros(3)
mL = np.zeros(3)
usr = np.zeros(3)
vsr = np.array([0,0,1])
g = np.zeros(3)

BDF = BDF2(dt)
MP = get_RP(L, r, E, nu, rho, g, usr, vsr, Bbt, Bse, BDF.c0)
hist = History(p0,R0,nL,mL,Nt,N)

run_carotid(hist,MP,BDF,CD_surface,k,dp,gamma,mu,eps)

return_folder_name = []
non_unique_name    = "_carotid_artery_insertion"
directory = "StorageData/carotid_artery_insertion/"
Storage(hist, MP, BDF, CD_surface, VZ_surface, k, return_folder_name, non_unique_name, directory)
storage_path = directory+return_folder_name[0]
carotid_artery_insertion_plot_3D(storage_path, return_folder_name[0], non_unique_name, VZ_surface, color = "green", opacity = 0.2)