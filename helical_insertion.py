import numpy as np
from Cosserat.RodProperties import *
from Simulation import *
from plot_3D import helical_insertion_plot_3D
from Geometry.BlobbyHelix import BlobbyHelix
from Geometry.MyTrimesh import MyTrimesh
from Storage import Storage
from Cosserat.History import *
from Misc.Other import *

use_implicit_surface = True

if use_implicit_surface:
    cube_size_divisor = 100 #used for surface visualization
else: #triangle mesh case
    # cube_size_divisor = 150 
    # cube_size_divisor = 100 
    # cube_size_divisor = 50  
    cube_size_divisor = 25  

#---------------------------------------------

L = 0.26
r = 0.0003
E = 5.5e+7
nu = 0.45
rho = 1100 
Bbt = 5e-7*np.eye(3) 
Bse = 5e-8*np.eye(3) 
dt = 0.005
NB = 2

helix_radius = 0.03
climb_rate = helix_radius/3
turns = 1
points_per_turn = 80
sphere_radius = helix_radius / 15
threshold = 0.0004*L  
blobby_helix = BlobbyHelix(threshold, sphere_radius, helix_radius, climb_rate, turns, points_per_turn, L)
_,dp = create_helix(helix_radius, climb_rate, turns, points_per_turn*NB, L)

cube_size = L/cube_size_divisor
bounds = np.array([[-0.01676949, -0.04580034, -0.27061106],
                    [ 0.07948793,  0.04665429,  0.27082183]])
vertices, faces, vnormals = mesh_from_blobby_model(blobby_helix.bm,bounds,cube_size,padding=0.0)
mymesh = MyTrimesh(vertices,faces,vnormals)
print("nb faces", faces.shape)

p0 = blobby_helix.helix_centers[0]
if use_implicit_surface: 
    CD_surface = blobby_helix
else: 
    CD_surface = mymesh 
VZ_surface = mymesh

Nt = 200
N = 125
k = 0.32

gamma = 5e-6
eps = 0 
mu = 0 
R0 = np.eye(3)
nL = np.zeros(3)
mL = np.zeros(3)
usr = np.zeros(3)
vsr = np.array([0,0,1])
g = np.zeros(3)

BDF = BDF2(dt)
MP = get_RP(L, r, E, nu, rho, g, usr, vsr, Bbt, Bse, BDF.c0)
hist = History(p0,R0,nL,mL,Nt,N)

run_helical_insertion(hist,MP,BDF,CD_surface,k,dp,gamma)

return_folder_name = []
if CD_surface == blobby_helix:
    non_unique_name    = "_helical_insertion_blobby_model"
else:
    non_unique_name    = "_helical_insertion_mesh_L_"+str(cube_size_divisor)
directory = "StorageData/helical_insertion/"
Storage(hist, MP, BDF, CD_surface, VZ_surface, k, return_folder_name, non_unique_name, directory)
storage_path = directory+return_folder_name[0]

helical_insertion_plot_3D(storage_path, return_folder_name[0], non_unique_name, VZ_surface, color = "green", opacity = 0.2)
