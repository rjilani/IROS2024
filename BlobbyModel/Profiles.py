import abc
import numpy as np
import scipy.optimize as scopt

# always normalized such that if g(x) = Profile.value(x*x)
# g''(1) = 0
# other characteristics:
# g(0) = 1
# g(x) = 1 (for x < 0)
# g'(0) = 0
# g(+infty) = g'(+infty) = 0

class Profile(object):
    """Abstract class to encode a profile radial basis function"""
    __metaclass__ = abc.ABCMeta
    @abc.abstractmethod
    def fvalue(self,x):
        """
        Evaluate the function value at x, with no test on the validity of x
        The validity if checked in value()
        """
        return

    def valueCheck(self, x, f):
        """Evaluate the function value at x"""
        if isinstance(x,np.ndarray):
            ret=np.ones(x.shape)
            i=np.where(x>0)
            ret[i] = f(x[i])
            return ret
        else:
            if x>0:
                return f(x)
            else:
                return 1

    def value(self,x):
        return self.valueCheck(x,self.fvalue)

    def __call__(self,x):
        return self.value(x)

    @abc.abstractmethod
    def invvalue(self, x):
        """
        Evaluate the inverse function value at x, with no test on the validity of x
        The validity if checked in inv()
        """
        return

    def inv(self, x):
        """Returns y such that y=Profile.value(x)"""
        print(x)
        if isinstance(x,np.ndarray):
            ret=np.ones(x.shape)*(-np.inf)
            i=np.where(x>0)
            ret[i] = self.invvalue(x[i])
            return ret
        else:
            if x>0:
                return self.invvalue(x)
            else:

                return -np.inf
        return

    @abc.abstractmethod
    def fdiff1(self, x):
        """
        Evaluate first derivative at x, with no test on the validity of x
        The validity if checked in diff1()
        """
        return

    def diff1(self, x):
        """Evaluate first derivative at x"""
        return self.valueCheck(x,self.fdiff1)


    @abc.abstractmethod
    def fdiff2(self, x):
        """
        Evaluate second derivative at x, with no test on the validity of x
        The validity if checked in diff2()
        """
        return

    def diff2(self, x):
        """Evaluate second derivative at x"""
        return self.valueCheck(x,self.fdiff2)


    @abc.abstractmethod
    def fdiff3(self, x):
        """
        Evaluate third derivative at x, with no test on the validity of x
        The validity if checked in diff3()
        """
        return

    def diff3(self, x):
        """Evaluate third derivative at x"""
        return self.valueCheck(x,self.fdiff3)

    @abc.abstractmethod
    def inflectingValue(self):
        """Value for x such that diff2(x) == 0"""
        return

    def radius(self,T, r):
        """
        Returns the param value x such that r*Profile(x^2/r^2) = T
        i.e. x = abs(r)*sqrt(Profile.inv(T/r))
        returns 0 if T/r < 0 or T/r > 1
        """
        if isinstance(r,np.ndarray):
            ret=np.zeros(r.shape)
            if T>0:
                i=np.where(r>T)[0]
            elif T<0:
                i=np.where(r<T)[0]
            else:
                return ret
            ret[i] = np.abs(r[i])*np.sqrt(self.inv(T/r[i]))
            return ret
        else:
            if r*T>0 and T/r<1:
                return np.abs(r)*np.sqrt(self.inv(T/r))
            else:
                return 0

    def radiusFromThreshold(self, T, R):
        """
        A blob function is defined as B(x;r)=r*Profile((x*x)/(r*r)).
        This function returns the value for r such that B(R;r)=T
        """
        s=scopt.fsolve(lambda r : r*self.value((R*R)/(r*r))-T,1)
        return s[np.where(s>=0)]

    @abc.abstractmethod
    def fdiffall(self,x):
        return

    @abc.abstractmethod
    def fdiff12(self,x):
        return

    

class Gaussian(Profile):
    def __str__(self):
        return "Gaussian"

    def fvalue(self, x): # exp(-0.5*x)
        return np.exp(-0.5*x)

    def invvalue(self, x): # -2 log(x)
        return -2*np.log(x)

    def fdiff1(self,x):
        return -0.5*np.exp(-0.5*x)

    def fdiff2(self, x):
        return 0.25*np.exp(-0.5*x)

    def fdiff3(self, x):
        return -0.125*np.exp(-0.5*x)

    def fdiffall(self,x):
        exp = np.exp(-0.5*x)
        return (exp, -0.5*exp, 0.25*exp, -0.125*exp)

    def fdiff12(self,x):
        exp = np.exp(-0.5*x)
        return (exp, -0.5*exp, 0.25*exp)

class Cauchy(Profile):
    def __str__(self):
        return "Cauchy"

    def fvalue(self, x): # 1/(x/5+1)**2
        return 1.0/(x/5 + 1)**2

    def fdiff1(self, x):
        return -(2.0/5.0)/(x/5+1)**3

    def fdiff2(self, x):
        return (6.0/25.0)/(x/5+1)**4

    def fdiff3(self, x):
        return -(24.0/125.0)/(x/5+1)**5

    def invvalue(self, x):
        return 5 * ((1/np.sqrt(x)) - 1)

    def fdiffall(self, x):
        val = 1 + (x/5)
        square = val * val
        cube = square * val
        fourth = cube * val
        fifth = square * cube
        return (1 / square, -(2.0/5.0)/cube, (6.0/25.0)/fourth, -(24.0/125.0)/fifth)

    def fdiff12(self, x):
        val = 1 + (x/5)
        square = val * val
        cube = square * val
        fourth = cube * val
        return (1 / square, -(2.0/5.0)/cube, (6.0/25.0)/fourth)

