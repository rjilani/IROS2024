import numpy as np
class Blob:
    """ Class to encode a single Blob """
    def __init__(self, center=np.zeros(3), radius=1):
        self.center = center
        self.radius = radius
    
    def __str__(self):
        """ Display its centre and radius when called as an str"""
        return f'{self.center[0]} {self.center[1]} {self.center[2]} {self.radius}'
    
    @property
    def center(self):
        return self._center
    
    @center.setter
    def center(self,center):
        if not isinstance(center,np.ndarray) or center.shape != (3,):
            raise ValueError
        self._center = center.copy()

    @property
    def radius(self):
        return self._radius
    
    @radius.setter
    def radius(self, radius):
        if not np.isscalar(radius) or radius <= 0:
            raise ValueError
        self._radius=radius
        self._radius2=radius*radius

    def toArray(self):
        return np.append(self.center,self.radius)

    def value(self, P, profile):
        """ Evaluate the function of the blob at points P. P must be 3xN """
        diff = (P.T - self.center).T
        x=np.sum(diff*diff,axis=0)/self._radius2
        return self.radius * profile.fvalue(x)
    
    def gradient(self, P, profile):
        """ Evaluate the gradient of the blob at the point P """
        diff = (P.T - self.center).T
        x=np.sum(diff*diff,axis=0)/self._radius2
        val = (2 / self.radius) * profile.fdiff1(x)
        return val * diff
    
    def valueNgradient(self, P, profile):
        diff = (P.T - self.center).T
        x=np.sum(diff*diff,axis=0)/self._radius2
        v,d=profile.fvdiff1(x)
        return (self.radius * v, d * diff)

    def my_valueNgradient(self, P, profile):
        diff = (P.T - self.center).T
        x=np.sum(diff*diff,axis=0)/self._radius2
        v = profile.fvalue(x)
        d = profile.fdiff1(x)
        return (self.radius * v, (2/self.radius)*d*diff)