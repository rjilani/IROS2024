from BlobbyModel.Blob import Blob
from BlobbyModel.Profiles import Profile, Gaussian, Cauchy
import numpy as np

class BM:
    """ Classe représentant un BM """
    def __init__(self, blobs=[], profile=Cauchy(), threshold=0.1):
        """ Initialisation d'un BM en prenant une liste de Blobs """
        self.blobs = blobs
        self._profile=profile
        self._threshold=threshold

    def __str__(self):
        res = f"Bloddy model with {len(self.blobs)} blob{'s' if len(self.blobs)>1 else ''}, "
        res += f"using profile {self.profile} and threshold {self.threshold}\n"
        res += "\n".join(f"Blob {i} : {b}" for i,b in enumerate(self.blobs))
        return res

    def __iter__(self):
        return iter(self.blobs)

    @property
    def profile(self):
        return self._profile
    @profile.setter
    def profile(self, profile):
        self._profile = profile

    @property
    def threshold(self):
        return self._threshold
    @threshold.setter
    def threshold(self,threshold):
        self._threshold=0 if threshold<0 else threshold

    @property
    def blobs(self):
        return self._blobs
    @blobs.setter
    def blobs(self, b):
        if not isinstance(b,list):
            raise ValueError
        self._blobs=b

    def append(self, b):
        if isinstance(b,Blob):
            self.blobs.append(b)

    def value(self,P):
        return np.sum([b.value(P,self.profile) for b in self.blobs], axis=0) - self.threshold

    def gradient(self, P):
        return np.sum([b.gradient(P,self.profile) for b in self.blobs], axis=0)
    
    def valueNgradient(self, P):
        vg=[b.valueNgradient(P,self.profile) for b in self.blobs]
        val=np.sum([v[0] for v in vg],axis=0) - self.threshold
        grad=np.sum([g[1] for g in vg],axis=0)
        return val, grad
    
    def my_valueNgradient(self, P):
        vg=[b.my_valueNgradient(P,self.profile) for b in self.blobs]
        val=np.sum([v[0] for v in vg],axis=0) - self.threshold
        grad=np.sum([g[1] for g in vg],axis=0)
        return val, grad
    
    def toFile(self,filename):
        """ Ecrit dans le fichier filename le BM au format .bm """
        with open(filename, "w") as f:
            f.write("\n".join(str(x) for x in self.blobs))

    def fromFile(self,filename):
        """ Lit le fichier filename au format .bm et écrit les informations dans le BM """
        with open(filename, "r") as f:
            self.fromArray([np.array(l.split(),dtype=float) for l in f.readlines()])
    
    def toArray(self):
        """ Renvoie une liste des coordonnées de chacun des blobs composant le BM """
        return np.array([b.toArray() for b in self.blobs])

    def fromArray(self, array):
        self.blobs = [Blob(l[:3],l[3]) for l in array if len(l)==4]

    def distanceTaubin(self,P):
        v=self.value(P)
        n=np.linalg.norm(self.gradient(P),axis=0)
        return v/n
