from Cosserat.ODE import *
from scipy.spatial.transform import Rotation

def residual(n0m0_ig, BC, MP, N, ODE, Zh = None, BDF = None, fc = None):
    Y = retrieve_Y(n0m0_ig, BC, MP, N, ODE, Zh, BDF, fc)
    nL = BC[12:15]
    mL = BC[15:18]
    nL_shot = Y[-1,12:15]
    mL_shot = Y[-1,15:18]
    resi = np.concatenate([nL_shot-nL, mL_shot-mL])
    return resi

def retrieve_Y(n0m0, BC, MP, N, ODE, Zh = None, BDF = None, fc = None):
    p0 = BC[0:3]
    R0 = BC[3:12]
    L = MP["L"]
    if Zh is None: #static case
        y0 = np.concatenate([p0,R0,n0m0])
        Y  = euler_ivp(ODE, (0,L), y0, N,args=([MP,Zh,BDF,fc,0]))
    else: #dynamic case with quaternion integration
        # Yh: [p(0:3), h(3:7), n(7:10), m(10:13), q(13:16), w(16:19)]
        h0_switched = Rotation.from_matrix(R0.reshape((3,3))).as_quat()
        h0 = np.zeros(4)
        h0[0]  = h0_switched[3]
        h0[1:] = h0_switched[0:3]

        q0w0 = BC[18:24]
        y0 = np.concatenate([p0,h0,n0m0,q0w0])
        Yh = euler_ivp(ODE, (0,L), y0, N,args=([MP,Zh,BDF,fc,0]))
        
        Y = np.zeros((N,24))
        Y[:,0:3]   = Yh[:,0:3]
        Y[:,12:24] = Yh[:,7:19]
        for j in range(N):
            R_j =  Rh(Yh[j,3:7])
            Y[j,3:12] = R_j.flatten()
    
    return Y

def retrieve_Y_Z_Zt(n0m0, BC, MP, N, ODE, Zh, BDF, fcs):
    c0            = BDF.c0
    Kbtusr        = MP["Kbtusr"]
    Ksevsr        = MP["Ksevsr"]
    inv_Kbt_c0Bbt = MP["inv_Kbt_c0Bbt"]
    inv_Kse_c0Bse = MP["inv_Kse_c0Bse"]
    Bbt           = MP["Bbt"]
    Bse           = MP["Bse"]

    Y = retrieve_Y(n0m0, BC, MP, N, ODE, Zh, BDF, fcs)

    Z = np.zeros((N,12))
    Zt = np.zeros((N,12))
    for j in range(N):
        R_j = Y[j,3:12].reshape((3,3))
        n_j = Y[j,12:15]
        m_j = Y[j,15:18]
        q_j = Y[j,18:21]
        w_j = Y[j,21:24]

        uh_j, qh_j, wh_j, vh_j = Zh[j, 0:3], Zh[j, 3:6], Zh[j, 6:9], Zh[j, 9:12]
        u_j = inv_Kbt_c0Bbt@(R_j.T@m_j+Kbtusr-Bbt@uh_j)
        v_j = inv_Kse_c0Bse@(R_j.T@n_j+Ksevsr-Bse@vh_j)

        ut_j = c0*u_j + uh_j
        qt_j = c0*q_j + qh_j
        wt_j = c0*w_j + wh_j
        vt_j = c0*v_j + vh_j

        Z[j]  = np.concatenate([u_j,q_j,w_j,v_j])
        Zt[j] = np.concatenate([ut_j,qt_j,wt_j,vt_j])

    return Y, Z, Zt

def retrieve_uv_static(Y, MP, N):
    usr    = MP["usr"]
    vsr    = MP["vsr"]
    invKbt = MP["invKbt"]
    invKse = MP["invKse"]
    u = np.zeros((N,3))
    v = np.zeros((N,3))
    for j in range(N):
        R_j    = Y[j,3:12].reshape((3,3))
        n_j    = Y[j,12:15]
        m_j    = Y[j,15:18]
        u[j] = invKbt@R_j.T@m_j + usr
        v[j] = invKse@R_j.T@n_j + vsr
    return u,v