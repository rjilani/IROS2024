import numpy as np
class BDF2:
    def __init__(self, dt):
        """
        dt : time step size
        c0,c1,c2 : BDF2 parameters
        """
        self.dt = dt
        self.c0 = 1.5/dt
        self.c1 = -2/dt
        self.c2 = 0.5/dt

    def get_Zh(self, i, Z_, u=None, v=None):
        c1 = self.c1
        c2 = self.c2
        if i == 0: 
            Zh         = np.zeros((u.shape[0],12))
            Zh[:,0:3]  = (c1+c2)*u
            Zh[:,9:12] = (c1+c2)*v
        elif i == 1:
            Zh = (c1+c2)*Z_[i-1]
        else:
            Zh = c1*Z_[i-1] + c2*Z_[i-2]
        return Zh
    



