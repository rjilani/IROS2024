import numpy as np
import time
from Misc.Math import *

def compute_contact_force(Y, Z, k, CD_surface, N, _0LSTEPS, mu, eps, gamma, g_dir, is_carotid = False):
        p = Y[:,0:3]
        CD_t0 = time.time()
        intersect, phi, normal = CD_surface.intersect_distance_normal(p)
        CD_t1 = time.time()
        CD_time = CD_t1-CD_t0
      
        CR_t0 = time.time()
        R = Y[:,3:12]
        q = Y[:,18:21]
        v = Z[:,9:12]
        fcbar = np.zeros((N,3))
        for j in range(N):
            if (not is_carotid and intersect[j]) or (is_carotid and intersect[j] and p[j,2] > 0): 
                  phi_j = phi[j]
                  normal_j = normal[j]
                  # The sign convention of implicit surfaces is changed compared to the IROS 2024 paper
                  if phi_j >= -gamma and phi_j < 0:
                        lambda_n_j = k*phi_j*phi_j/(2*gamma)
                  elif phi_j < -gamma:
                        lambda_n_j = -k*(phi_j+gamma/2)
                  fbar_n_j = lambda_n_j*normal_j
                  if mu == 0:
                        fbar_t_j = np.zeros(3)
                  else:
                        R_j = R[j].reshape((3,3))
                        v_j = v[j]
                        q_j = q[j]
                        fbar_t_j = compute_friction_force(lambda_n_j, v_j, q_j, R_j, normal_j, mu, eps, g_dir)
                  fcbar[j] = fbar_n_j + fbar_t_j
        nL = fcbar[-1]
        ds = _0LSTEPS[1]
        fc = fcbar/ds
        CR_t1 = time.time()
        CR_time = CR_t1-CR_t0

        return nL, fc, intersect, CD_time, CR_time

def compute_friction_force(lambda_n,v,q,R,normal,mu,eps,g_dir):
        ps = R@v
        a = np.cross(ps,normal) 
        b = np.cross(normal,a)  

        pt = R@q
        pt_a = np.dot(pt,a)*a
        pt_b = np.dot(pt,b)*b
        pt_ab = pt_a+pt_b     
        norm_pt_ab = np.linalg.norm(pt_ab)

        f_ab = np.zeros(3)
        if norm_pt_ab > eps:
                if norm_pt_ab > 0:
                        f_ab = -mu*lambda_n*pt_ab/norm_pt_ab
        else:
                g_a = np.dot(g_dir,a)*a
                g_b = np.dot(g_dir,b)*b
                g_ab = g_a+g_b     
                norm_g_ab = np.linalg.norm(g_ab)
                if norm_g_ab > 0:
                        f_ab = -mu*lambda_n*g_ab/norm_g_ab
        return f_ab

