import numpy as np


class History:
    def __init__(self,p0,R0,nL,mL,Nt,N):
        # Y : [p(0:3),R(3:12),n(12:15),m(15:18),q(18:21),w(21:24)]
        # Z : [u(0:3),q(3:6),w(6:9),v(9:12)]
        # Y and Z conventions in the code were taken from Till et al. 2019 
        
        self.Nt = Nt
        self.N  = N

        self.Y_rest       = np.zeros((N,18))
        self.sol_rest     = None
        self.BC_rest      = np.concatenate([p0,R0.flatten(),nL,mL])
        self.SM_time_rest = 0

        self.Yu_  = np.zeros((Nt,N,24))
        self.Yc_  = np.zeros((Nt,N,24))
        
        self.Zu_  = np.zeros((Nt,N,12))
        self.Zc_  = np.zeros((Nt,N,12))
        
        self.Ztu_ = np.zeros((Nt,N,12))
        self.Ztc_ = np.zeros((Nt,N,12))
        self.Zh_     = np.zeros((Nt,N,12)) 

        self.BCu_ = np.zeros((Nt,24))  
        self.BCc_ = np.zeros((Nt,24))  
        
        self.solu_ = np.empty(Nt,dtype=object)
        self.solc_ = np.empty(Nt,dtype=object)

        self.fc_     = np.zeros((Nt,N,3))  

        self.SM_timeu_ = np.zeros(Nt)
        self.SM_timec_ = np.zeros(Nt) 
        self.CD_time_ = np.zeros(Nt)
        self.CR_time_ = np.zeros(Nt)
        
        self.in_col_ = np.zeros((Nt,N))   