import numpy as np

def get_RP(L, r, E, nu, rho, g, usr, vsr, Bbt, Bse, c0):
    A   = np.pi*r**2     
    G   = E/(2*(1+nu))   
    I1 = np.pi*r**4/4
    I2 = I1
    I3  = 2*I1

    EI  = E*I1
    GI3 = G*I3
    GA  = G*A
    EA  = E*A

    J   = np.diag([I1,I2,I3])
    Kbt = np.diag([EI, EI, GI3])
    Kse = np.diag([GA, GA, EA])
    invKbt = np.linalg.inv(Kbt)
    invKse = np.linalg.inv(Kse)
    inv_Kbt_c0Bbt = np.linalg.inv(Kbt + c0*Bbt)
    inv_Kse_c0Bse = np.linalg.inv(Kse + c0*Bse)
    Kbtusr = Kbt@usr
    Ksevsr = Kse@vsr

    rhoAg = rho*A*g
    rhoA  = rho*A

    MP = {}

    MP["L"]  = L
    MP["r"]  = r
    MP["E"]  = E
    MP["nu"] = nu
    MP["rho"] = rho
    MP["g"]  = g
    MP["usr"] = usr
    MP["vsr"]  = vsr
    MP["Bbt"] = Bbt
    MP["Bse"] = Bse
    # MP["G"]  = G
    MP["J"]      = J
    MP["Kbt"] = Kbt
    MP["Kse"] = Kse
    MP["invKbt"] = invKbt
    MP["invKse"] = invKse
    MP["inv_Kbt_c0Bbt"] = inv_Kbt_c0Bbt
    MP["inv_Kse_c0Bse"] = inv_Kse_c0Bse
    MP["Kbtusr"] = Kbtusr
    MP["Ksevsr"] = Ksevsr
    MP["rhoA"]   = rhoA
    MP["rhoAg"]  = rhoAg

    return MP