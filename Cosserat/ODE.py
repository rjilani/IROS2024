import numpy as np
from Misc.Math import *

# orientations integration with quaternion
# Y: [p(0:3), h(3:7), n(7:10), m(10:13), q(13:16), w(16:19)], h being the quaternion
def ODE_dynamic(s, Y, MP, Zh, BDF, fc, j):
    del s

    rho   = MP["rho"] 
    rhoA  = MP["rhoA"]
    rhoAg = MP["rhoAg"]
    Bbt = MP["Bbt"]
    Bse = MP["Bse"]
    Kbtusr = MP["Kbtusr"]
    Ksevsr = MP["Ksevsr"]
    inv_Kbt_c0Bbt = MP["inv_Kbt_c0Bbt"]
    inv_Kse_c0Bse = MP["inv_Kse_c0Bse"]
    J     = MP["J"]
    c0    = BDF.c0

    h = Y[3:7]
    R = Rh(h)
    n = Y[7:10]
    m = Y[10:13]
    q = Y[13:16]
    w = Y[16:19]
    
    uh = Zh[j][0:3]
    qh = Zh[j][3:6]
    wh = Zh[j][6:9]
    vh = Zh[j][9:12]

    fc = fc[j] 
    f  = rhoAg + fc

    u  = inv_Kbt_c0Bbt@(R.T@m+Kbtusr-Bbt@uh)
    v  = inv_Kse_c0Bse@(R.T@n+Ksevsr-Bse@vh)

    u_hat = hat(u)
    w_hat = hat(w)

    ut = c0*u + uh
    qt = c0*q + qh
    wt = c0*w + wh
    vt = c0*v + vh

    ps  = R@v
    ps_hat = hat(ps)
    hs  = 0.5*hat_for_h(u)@h
    ns  = R@(rhoA*(w_hat@q + qt)) - f
    ms  = rho*R@(w_hat@J@w + J@wt) - ps_hat@n #-l
    qs  = vt - u_hat@q + w_hat@v
    ws  = ut - u_hat@w

    Ys = np.concatenate([ps,hs,ns,ms,qs,ws])
    return  Ys

# Y : [p(0:3), R(3:12), n(12:15), m(15:18)]
# orientations integration with rotation matrices (in our numerical applications the rod is straight in rest configuration)
def ODE_static(s, Y, MP, none1, none2, none3, none4):
    del s
    f = MP["rhoAg"]
    usr = MP["usr"]
    vsr = MP["vsr"]
    invKbt = MP["invKbt"]
    invKse = MP["invKse"]

    R = Y[3:12].reshape((3,3))
    n = Y[12:15]
    m = Y[15:18]

    u = invKbt@R.T@m+usr
    v = invKse@R.T@n+vsr
    u_hat = hat(u)

    ps  = R@v
    Rs  = R@u_hat
    ns  = -f
    ps_hat = hat(ps)
    ms  = -ps_hat@n #-l
    Ys  = np.concatenate([ps,Rs.flatten(),ns,ms])
    return Ys