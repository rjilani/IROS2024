from scipy.optimize import root
from Cosserat.Residual import *
from Cosserat.BDF2 import *
import time as time
from Cosserat.ContactHandling import *

def run_carotid(hist,MP,BDF,CD_surface,k,dp,gamma,mu,eps):
    N = hist.N

    g_dir = np.zeros(3)
    _0LSTEPS  = np.linspace(0,MP["L"],N)
    fcu = np.zeros((N,3))

    hist.Y_rest, hist.sol_rest, hist.SM_time_rest = do_static_step(-1,np.zeros(6),hist.BC_rest,MP,N)
    u_rest, v_rest = retrieve_uv_static(hist.Y_rest, MP, N)

    BCu = np.concatenate([hist.BC_rest, np.zeros(6)]) #p0,R0,nL,mL,q0,w0
    sol = hist.sol_rest
    solu = sol

    for i in range(hist.Nt):
        Zh = BDF.get_Zh(i, hist.Zc_, u_rest, v_rest)  
        BCu[0:3]  += dp[i]   
        
        Yu, Zu, Ztu, solu, t1_t0u = do_dynamic_step(i, solu.x, fcu, Zh, BCu, BDF, MP, N, False)
        nL, fc, in_col, CD_time, CR_time = compute_contact_force(Yu, Zu, k, CD_surface, N, _0LSTEPS, mu, eps, gamma, g_dir, is_carotid=True)
        store_step(0, i, hist, Yu, Zu, Ztu, BCu, solu, t1_t0u)

        print("contact detection time", CD_time)

        BCc = np.copy(BCu)
        BCc[12:15] = nL
        Y, Z, Zt, sol, t1_t0 = do_dynamic_step(i, sol.x, fc, Zh, BCc, BDF, MP, N, True)
        store_step(1, i, hist, Y, Z, Zt, BCc, sol, t1_t0, in_col, fc, Zh, CD_time, CR_time) 

def run_helical_insertion(hist,MP,BDF,CD_surface,k,dp,gamma):
    N  = hist.N
    dt = BDF.dt
    
    mu = 0
    eps = 0
    g_dir = np.zeros(3)
    fcu = np.zeros((N,3))
    _0LSTEPS  = np.linspace(0,MP["L"],N)

    hist.Y_rest, hist.sol_rest, hist.SM_time_rest = do_static_step(-1,np.zeros(6),hist.BC_rest,MP,N)
    u_rest,v_rest = retrieve_uv_static(hist.Y_rest,MP,N)

    BCu = np.concatenate([hist.BC_rest, np.zeros(6)]) 
    sol = hist.sol_rest
    solu = sol

    for step in range(hist.Nt):
        Zh = BDF.get_Zh(step, hist.Zc_, u_rest, v_rest)  

        BCu[0:3]  += dp[step]   
        BCu[18:21] = dp[step]/dt #since R.T=R=I

        Yu, Zu, Ztu, solu, t1_t0u = do_dynamic_step(step, solu.x, fcu, Zh, BCu, BDF, MP, N, False)
        nL, fc, in_col, CD_time, CR_time = compute_contact_force(Yu, Zu, k, CD_surface, N, _0LSTEPS, mu, eps, gamma, g_dir)
        store_step(0, step, hist, Yu, Zu, Ztu, BCu, solu, t1_t0u)

        print("contact detection time", CD_time)

        BCc = np.copy(BCu)
        BCc[12:15] = nL
        Y, Z, Zt, sol, t1_t0 = do_dynamic_step(step, sol.x, fc, Zh, BCc, BDF, MP, N, True)
        store_step(1, step, hist, Y, Z, Zt, BCc, sol, t1_t0, in_col, fc, Zh, CD_time, CR_time) 

def run_cantilever_rod_in_frictional_contact(hist,MP,BDF,CD_surface,k,mu,eps,gamma,dynamic_g):
    N  = hist.N

    fcu = np.zeros((N,3))
    _0LSTEPS  = np.linspace(0,MP["L"],N)
    g_dir = dynamic_g/np.linalg.norm(dynamic_g)
    n0m0_ig   = np.zeros(6)

    hist.Y_rest, hist.sol_rest, hist.SM_time_rest = do_static_step(-1,n0m0_ig,hist.BC_rest,MP,N)
    u_rest,v_rest = retrieve_uv_static(hist.Y_rest,MP,N)

    MP["rhoAg"] = MP["rhoA"]*dynamic_g
    BCu = np.concatenate([hist.BC_rest, np.zeros(6)]) 
    sol = hist.sol_rest
    solu = sol

    for step in range(hist.Nt):
        Zh = BDF.get_Zh(step, hist.Zc_, u_rest, v_rest)

        Yu, Zu, Ztu, solu, t1_t0u = do_dynamic_step(step, solu.x, fcu, Zh, BCu, BDF, MP, N, False)
        nL, fc, penu, CD_time, CR_time = compute_contact_force(Yu, Zu, k, CD_surface, N, _0LSTEPS, mu, eps, gamma, g_dir)
        store_step(0, step, hist, Yu, Zu, Ztu, BCu, solu, t1_t0u, penu)

        BCc = np.copy(BCu)
        BCc[12:15] = nL
        Y, Z, Zt, sol, t1_t0 = do_dynamic_step(step, sol.x, fc, Zh, BCc, BDF, MP, N, True)
        store_step(1, step, hist, Y, Z, Zt, BCc, sol, t1_t0, penu, fc, Zh, CD_time, CR_time) 

def do_static_step(step,n0m0_ig,BC_rest,MP,N):
    t0 = time.time()
    sol = root(residual,n0m0_ig,args=(BC_rest,MP,N,ODE_static))
    t1 = time.time()
    print_sol(sol,t1-t0,step,False)    
    Y_rest = retrieve_Y(sol.x, BC_rest, MP, N, ODE_static)
    return Y_rest, sol, t1-t0

def do_dynamic_step(step, n0m0_ig, fcs, Zh, BC, BDF, MP, N,constrained):
    t0 = time.time()
    sol = root(residual,n0m0_ig,args=(BC,MP,N,ODE_dynamic,Zh,BDF,fcs))
    t1 = time.time()
    print_sol(sol,t1-t0,step,constrained)   
    Y, Z, Zt = retrieve_Y_Z_Zt(sol.x, BC, MP, N, ODE_dynamic, Zh, BDF, fcs)
    return Y, Z, Zt, sol, t1-t0

def store_step(constrained, i, hist, Y, Z, Zt, BC, sol, t1_t0, in_col = None, fc=None, Zh=None, CD_time = None, CR_time = None):
    if not constrained:
        hist.Yu_[i] = Y
        hist.Zu_[i] = Z
        hist.Ztu_[i] = Zt
        hist.BCu_[i] = BC
        hist.solu_[i] = sol
        hist.SM_timeu_[i] = t1_t0
    else:
        hist.Yc_[i] = Y
        hist.Zc_[i] = Z
        hist.Ztc_[i] = Zt
        hist.BCc_[i] = BC
        hist.solc_[i] = sol
        hist.SM_timec_[i] = t1_t0
        hist.in_col_[i] = in_col
        hist.fc_[i] = fc
        hist.Zh_[i] = Zh    
        hist.CD_time_[i] = CD_time
        hist.CR_time_[i] = CR_time

def print_sol(sol,exec_time,step,constrained):
    print("---------------------------------------------------")
    print("time step:", step)
    print("converged:", sol.success)
    print("max abs error:", np.max(np.abs(sol.fun)))
    print("xi (nfev):", sol.nfev)
    if constrained : 
        print("contrained BVP solver time:", exec_time)
    else:
        print("uncontrained BVP solver time:", exec_time)
