import numpy as np
from stl import mesh
import math
from skimage import measure
import plotly.graph_objects as go

def create_helix(helix_radius, climb_rate, turns, points_per_turn, straight_line_length):
    t_values_helix = np.linspace(0, turns * 2 * np.pi, turns * points_per_turn)

    centers_helix = np.array([climb_rate * t_values_helix,
                            helix_radius * np.cos(t_values_helix),
                            helix_radius * np.sin(t_values_helix)]).T
    
    n_line_points = math.ceil(straight_line_length/np.linalg.norm(centers_helix[1]-centers_helix[0]))

    start_point = np.array([0, helix_radius, 0])  
    end_point = np.array([climb_rate * turns * 2 * np.pi, helix_radius * np.cos(turns * 2 * np.pi), helix_radius * np.sin(turns * 2 * np.pi)])  

    t_values_line1 = np.linspace(-straight_line_length, 0, n_line_points)
    centers_line1 = np.array([np.zeros_like(t_values_line1), 
                            np.zeros_like(t_values_line1), 
                            t_values_line1]).T + start_point

    t_values_line2 = np.linspace(0, straight_line_length, n_line_points)
    centers_line2 = np.array([np.zeros_like(t_values_line2), 
                            np.zeros_like(t_values_line2), 
                            t_values_line2]).T + end_point

    centers = np.concatenate((centers_line1[0:-1], centers_helix, centers_line2[1:]))
    displacements = centers[1:,:]-centers[0:-1,:]
    return centers, displacements


def create_stl(vertices, faces, flip_normal = False, path = "my_mesh"):
    num_faces = faces.shape[0]
    res_mesh = mesh.Mesh(np.zeros(num_faces, dtype=mesh.Mesh.dtype))
    for i, f in enumerate(faces):
        for j in range(3):  
            if not flip_normal:
                res_mesh.vectors[i][j] = vertices[f[j],:]
            else:
                res_mesh.vectors[i][j] = vertices[f[2-j],:]
    res_mesh.save(path+'.stl')

def mesh_from_blobby_model(bm, bounds, cube_size, padding):
    P,X,Y,Z,x,y,z = generate_grid_points(bounds, cube_size, padding)
    data = bm.value(P).reshape(X.shape)  
    vertices, faces, vnormals, _ = measure.marching_cubes(data, level=0)
    grid_scale = [np.ptp(x), np.ptp(y), np.ptp(z)]  
    grid_size = np.array(data.shape) - 1  
    vertices_scaled = vertices * (grid_scale / grid_size)
    origin_offset = [x.min(), y.min(), z.min()]  
    vertices_corrected = vertices_scaled + origin_offset
    return vertices_corrected, faces, vnormals

def generate_grid_points(bounds, cube_size, padding=0.0):
    min_corner, max_corner = bounds
    min_corner -= padding  
    max_corner += padding
    
    num_points_x = int(np.ceil((max_corner[0] - min_corner[0]) / cube_size))
    num_points_y = int(np.ceil((max_corner[1] - min_corner[1]) / cube_size))
    num_points_z = int(np.ceil((max_corner[2] - min_corner[2]) / cube_size))
    
    x = np.linspace(min_corner[0], max_corner[0], num_points_x)
    y = np.linspace(min_corner[1], max_corner[1], num_points_y)
    z = np.linspace(min_corner[2], max_corner[2], num_points_z)
    
    X, Y, Z = np.meshgrid(x, y, z, indexing='ij')
    points = np.vstack([X.ravel(), Y.ravel(), Z.ravel()])
    return points,X,Y,Z,x,y,z

def create_go_Mesh3d(vertices, faces, color, opacity):
     goMesh3d = go.Mesh3d(
                    x=vertices[:,0],
                    y=vertices[:,1],
                    z=vertices[:,2],
                    i=faces[:,0],
                    j=faces[:,1],
                    k=faces[:,2],
                    color=color,    
                    opacity=opacity,
                    lighting=dict( 
                        ambient=0.5, 
                        diffuse=1,  
                        fresnel=0.3,  
                        specular=1,  
                        roughness=0.3,  
                        facenormalsepsilon=1e-15,  
                        vertexnormalsepsilon=1e-15,  
                    ),
                    lightposition=dict(
                        x=10,  
                        y=20,  
                        z=30  
                    )
                )
     return  goMesh3d         
