import numpy as np
import math

def euler_ivp(fun, t_span, y0, N, args=()):
    t_start, t_end = t_span
    t_steps = np.linspace(t_start, t_end, N) 
    ds = t_steps[1]

    y = np.zeros((N, len(y0)))
    y[0] = y0 
    for i in range(N-1):
        t_i = t_steps[i]
        y_i = y[i]
        args[-1] = i #get the sj step to unpack Zh and fc
        ys_i = fun(t_i, y_i, *args)
        y[i+1] = y_i + ds*ys_i
    return y

def hat(u):
    return np.array([[0, -u[2], u[1]],
                    [u[2], 0, -u[0]],
                    [-u[1], u[0], 0]])

def hat_for_h(u):
    u1, u2, u3 = u
    return np.array([
            [0, -u1, -u2, -u3],
            [u1, 0, u3, -u2],
            [u2, -u3, 0, u1],
            [u3, u2, -u1, 0]
            ])

def Rh(h):
    h1, h2, h3, h4 = h
    h_squared = np.dot(h, h)
    I = np.eye(3)
    h2_2 = h2**2
    h3_2 = h3**2
    h4_2 = h4**2
    h2h3 = h2*h3
    h4h1 = h4*h1
    h2h4 = h2*h4
    h3h1 = h3*h1
    h3h4 = h3*h4
    h2h1 = h2*h1
    return I + 2 / h_squared * np.array([
        [-h3_2 - h4_2, h2h3 - h4h1, h2h4 + h3h1],
        [h2h3 + h4h1, -h2_2 - h4_2, h3h4 - h2h1],
        [h2h4 - h3h1, h3h4 + h2h1, -h2_2 - h3_2]
    ])

def rotation_matrix(axis, angle_degrees):
    angle_radians = np.radians(angle_degrees)
    cos_a = np.cos(angle_radians)
    sin_a = np.sin(angle_radians)
    if axis == 'x':
        return np.array([[1, 0, 0],
                         [0, cos_a, -sin_a],
                         [0, sin_a, cos_a]])
    elif axis == 'y':
        return np.array([[cos_a, 0, sin_a],
                         [0, 1, 0],
                         [-sin_a, 0, cos_a]])
    elif axis == 'z':
        return np.array([[cos_a, -sin_a, 0],
                         [sin_a, cos_a, 0],
                         [0, 0, 1]])