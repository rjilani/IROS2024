import matplotlib.pyplot as plt
import pickle
import Storage
import numpy as np

def get_storage(foldername):
    with open('StorageData/cantilever_rod_in_frictional_contact/'+foldername, 'rb') as file: 
        storage = pickle.load(file)
    return storage

if __name__ == "__main__":
    #paste .pkl files name
    filename0 = "97153947-955a-455d-b468-97de2c22d013_cantilever_rod_in_frictional_contact_0.pkl"   #for mu = 0 
    filename1 = "3b2d00ae-3756-4f76-8fc5-f3b3a6ce6766_cantilever_rod_in_frictional_contact_0.1.pkl" #for mu = 0.1
    filename2 = "4c2c257c-58b1-46db-b9e0-8cba59dc031b_cantilever_rod_in_frictional_contact_0.2.pkl" #for mu = 0.2
    filename3 = "76084411-cc20-4a96-8bad-01609066ce31_cantilever_rod_in_frictional_contact_0.3.pkl" #for mu = 0.3
    filename4 = "3b5adde2-920f-4ad1-96b0-5aa7ab43c531_cantilever_rod_in_frictional_contact_0.4.pkl" #for mu = 0.4

    storage0 =  get_storage(filename0)
    storage1 =  get_storage(filename1)
    storage2 =  get_storage(filename2)
    storage3 =  get_storage(filename3)
    storage4 =  get_storage(filename4)

    time0  = storage0.SM_timeu_ + storage0.SM_time_
    time1 = storage1.SM_timeu_ + storage1.SM_time_
    time2 = storage2.SM_timeu_ + storage2.SM_time_
    time3 = storage3.SM_timeu_ + storage3.SM_time_
    time4 = storage4.SM_timeu_ + storage4.SM_time_

    SM_avg = (sum(time0)+sum(time1)+sum(time2)+sum(time3)+sum(time4))/(5*200)
    print("Cantilever rod in frictional contact:")
    print("Avg. BVP solver time",SM_avg)

    fig, ax = plt.subplots()
    ax.plot(storage0.p_[-1,:,2],storage0.p_[-1,:,0],label=r'$\mu = 0.0$')
    ax.plot(storage1.p_[-1,:,2],storage1.p_[-1,:,0],label=r'$\mu = 0.1$')
    ax.plot(storage2.p_[-1,:,2],storage2.p_[-1,:,0],label=r'$\mu = 0.2$')
    ax.plot(storage3.p_[-1,:,2],storage3.p_[-1,:,0],label=r'$\mu = 0.3$')
    ax.plot(storage4.p_[-1,:,2],storage4.p_[-1,:,0],label=r'$\mu = 0.4$')

    radius = storage0.CD_surface.radius  
    x0, y0 = storage0.CD_surface.center[0], storage0.CD_surface.center[2]  
    theta = np.linspace(0, 2*np.pi, 100)
    x = radius * np.cos(theta) + x0
    y = radius * np.sin(theta) + y0
    ax.plot(y, x, label=f'sphere', color="black")
    plt.gca().invert_yaxis()
    ax.set_aspect('equal', 'box')
    ax.set_xlabel('z [m]')
    ax.set_ylabel('x [m]')
    ax.legend()
    ax.grid()
    # fig.savefig("_images\\cantilever_rod_in_frictional_contact\cantilever_rod_in_frictional_contact.eps", dpi=300, bbox_inches='tight')
    # fig.savefig("_images\\cantilever_rod_in_frictional_contact\cantilever_rod_in_frictional_contact.png", dpi=300, bbox_inches='tight')
    plt.show()
