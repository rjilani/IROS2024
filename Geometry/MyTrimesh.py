import trimesh
from Misc.Other import create_go_Mesh3d
import numpy as np

class MyTrimesh:
    def __init__(self, vertices, faces, vertex_normals = None, stl_path = None):
        if stl_path != None:
                self.mytrimesh = trimesh.load(stl_path+".stl")
        else:
            if np.any(vertex_normals) == None:
                self.mytrimesh = trimesh.Trimesh(vertices=vertices, faces=faces, process=False)
            else:
                self.mytrimesh = trimesh.Trimesh(vertices=vertices, faces=faces, vertex_normals=vertex_normals, process=False)
        self.fnormals = self.mytrimesh.face_normals

    def intersect_distance_normal(self, p):
        _, distances, triangles_id = trimesh.proximity.closest_point(self.mytrimesh, p)
        intersect = ~self.mytrimesh.contains(p)
        normals = self.fnormals[triangles_id]
        distances = -distances
        return intersect, distances, normals 

    def plot_3D(self, color, opacity, stl_path = None):
        if stl_path == None:
            goMesh3d = create_go_Mesh3d(self.mytrimesh.vertices, self.mytrimesh.faces, color, opacity)
        else:
            vizu_trimesh = trimesh.load(stl_path)
            goMesh3d = create_go_Mesh3d(vizu_trimesh.vertices, vizu_trimesh.faces, color, opacity)
        return goMesh3d