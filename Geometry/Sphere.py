import numpy as np
import plotly.graph_objects as go

class Sphere:
    def __init__(self, center, radius, intersect_if_outside):
        self.center = np.array(center)
        self.radius = radius
        self.intersect_if_outside = intersect_if_outside

    def intersect_distance_normal(self, p):
        vectors = self.center - p 
        distances_to_center = np.linalg.norm(vectors, axis=1)
        normals = vectors / (distances_to_center[:, np.newaxis] + np.finfo(float).eps) #always inward
        distances_to_surface = self.radius - distances_to_center 
        if self.intersect_if_outside:
            intersections = distances_to_surface <= 0
        else: #intersection if inside
            intersections = distances_to_surface >= 0
            distances_to_surface = -distances_to_surface #changed in order to compute fc 
            normals = -normals
        return intersections, distances_to_surface, normals 
    
    def plot_3D(self, color, opacity, grid_spane = np.pi, grid_size = 100):
        center = self.center
        radius = self.radius

        theta = np.linspace(0, grid_spane, grid_size)
        phi = np.linspace(0, 2 * grid_spane, grid_size)
        theta, phi = np.meshgrid(theta, phi)

        x = center[0] + radius * np.sin(theta) * np.cos(phi)
        y = center[1] + radius * np.sin(theta) * np.sin(phi)
        z = center[2] + radius * np.cos(theta)

        return go.Surface(x=x, y=y, z=z, colorscale=[[0, color], [1, color]], opacity=opacity, showlegend=False, name="Sphere")