import numpy as np
import plotly.graph_objects as go
from BlobbyModel.Blob import Blob
from BlobbyModel.BM import BM
import trimesh
from Misc.Other import *

class BlobbyCarotid:
    def __init__(self, N, n_neigh = 3, NBM = 70):
        self.N = N
        self.n_neigh = n_neigh
        self.NBM = NBM
    
        translation_vector = -np.array([39.88, 83.468, 1.0864])
        scale = 1/1000.0 #scaling from mm to m
        with open('Misc/bmShort.tree','r') as f:
            n=int(f.readline())
            bm_tree=[{} for i in range(n)]
            for i in range(n):
                bm_tree[i]['center']= np.array(f.readline().split(),dtype=np.float32)
                bm_tree[i]['center'][0:3] +=translation_vector
                bm_tree[i]['center']      *= scale
            for i in range(n):
                bm_tree[i]['next'] = np.array(f.readline().split(),dtype=np.uint32)[1:]
            for i in range(n):
                m=int(f.readline())
                bm_tree[i]['blobs_params'] = np.zeros((m,4))
                bm_tree[i]['blobs'] = [None] * m
                for j in range(m):
                    bm_tree[i]['blobs_params'][j] = np.array(f.readline().split(),dtype=np.float32)
                    bm_tree[i]['blobs_params'][j][0:3] +=translation_vector
                    bm_tree[i]['blobs_params'][j]      *= scale
                    blob_center = bm_tree[i]['blobs_params'][j][0:3]
                    blob_radius = bm_tree[i]['blobs_params'][j][3]
                    bm_tree[i]['blobs'][j] = Blob(blob_center,blob_radius)
                bm_tree[i]['bm'] = BM(bm_tree[i]['blobs'], threshold=0.1*scale) 

        self.bm_tree = bm_tree[0:NBM]

        self.min_nb_blobs = np.inf
        self.max_nb_blobs = -np.inf
        sum_nb_blobs = 0
        for i in range(NBM):
            m = len(bm_tree[i]['blobs'])
            sum_nb_blobs += m
            if m < self.min_nb_blobs:
                self.min_nb_blobs = m
            if m > self.max_nb_blobs:
                self.max_nb_blobs = m
        self.avg_nb_blobs = sum_nb_blobs/NBM

        self.bm_t_1 = None

    def intersect_distance_normal(self, p):
        intersect = np.zeros(self.N)
        distanceTaubin = np.zeros(self.N)
        normal = np.zeros((self.N,3))
        if np.all(self.bm_t_1) == None:
            self.bm_t_1 = np.zeros(self.N)
            for i in range(self.N):
                min_dist = np.inf
                id__bm_min_dist = -1
                for j in range(0, self.NBM):
                        dist = np.linalg.norm(p[i]-self.bm_tree[j]['center'][0:3])
                        if dist < min_dist:
                            min_dist = dist
                            id__bm_min_dist = j
                idn = self.intersect_distance_normal_local(p[i],self.bm_tree[id__bm_min_dist]['bm'])
                intersect[i], distanceTaubin[i], normal[i] = idn[0], idn[1], idn[2] 
                self.bm_t_1[i] = id__bm_min_dist
        else:
            n_neigh = self.n_neigh
            for i in range(self.N):
                id_bm_t_1 = int(self.bm_t_1[i])
                min_dist = np.inf
                id__bm_min_dist = -1
                for j in range(id_bm_t_1-n_neigh, id_bm_t_1+n_neigh+1):
                    if j >= 0 and j < self.NBM:
                        dist = np.linalg.norm(p[i]-self.bm_tree[j]['center'][0:3])
                        if dist < min_dist:
                            min_dist = dist
                            id__bm_min_dist = j
                idn = self.intersect_distance_normal_local(p[i],self.bm_tree[id__bm_min_dist]['bm'])
                intersect[i], distanceTaubin[i], normal[i] = idn[0], idn[1], idn[2] 
                self.bm_t_1[i] = id__bm_min_dist
        return intersect, distanceTaubin, normal
    
    def intersect_distance_normal_local(self, p, bm):
        val, grad = bm.my_valueNgradient(p)
        norm_grad = np.linalg.norm(grad,axis=0)
        distanceTaubin = val/norm_grad 
        intersect = distanceTaubin <= 0 
        normal = grad/norm_grad 
        return intersect, distanceTaubin, normal 
    
    def plot_3D(self, color , opacity, stl_path):
        mytrimesh = trimesh.load(stl_path)
        goMesh3d = create_go_Mesh3d(mytrimesh.vertices, mytrimesh.faces, color, opacity)
        return goMesh3d
    