import numpy as np
import plotly.graph_objects as go
from BlobbyModel.Blob import Blob
from BlobbyModel.BM import BM
from Misc.Other import *
import trimesh
class BlobbyHelix:
    def __init__(self, threshold, sphere_radius, helix_radius, climb_rate, turns, points_per_turn, straight_line_length):
        self.sphere_radius = sphere_radius
        self.helix_radius = helix_radius
        self.climb_rate = climb_rate
        self.turns = turns
        self.points_per_turn = points_per_turn
        self.straight_line_length = straight_line_length
        self.helix_centers, _ = create_helix(helix_radius, climb_rate, turns, points_per_turn, straight_line_length)
        blobs = [Blob(center, sphere_radius) for center in self.helix_centers]
        self.bm = BM(blobs, threshold=threshold)

    def intersect_distance_normal(self, p):
        val, grad = self.bm.my_valueNgradient(p.T) 
        norm_grad = np.linalg.norm(grad,axis=0)
        distanceTaubin = val/norm_grad
        intersect = [d <= 0 for d in distanceTaubin]
        normal = grad/norm_grad 
        return intersect, distanceTaubin, normal.T 
    
    def plot_3D(self, color, opacity, stl_path):
        mytrimesh = trimesh.load(stl_path)
        goMesh3d = create_go_Mesh3d(mytrimesh.vertices, mytrimesh.faces, color, opacity)
        return goMesh3d